# Wardrobify

React: the React-based front-end application where both you and your teammate will write the components to interact with your services
Shoes API: the RESTful API to interact with Shoe resources
Shoes Poller: the poller to poll the Wardrobe API for Bin resources

completed:
* [x]: Database: the PostgreSQL database that will hold the data of all of the microservices
* [x]: docker volume create pgdata???? is this the command that provides the database? (i think so because it generates the db/volume thing inside docker which is hosted locally)
* [x]: configure settings in hats_project to show installed app for HatsApiConfig from hats_rest
* [x]: check out wardrobe api information, models information to determine classes we’re constructing the foreignkeys from for hats
* [x]: Wardrobe API: provides Location and Bin RESTful API endpoints (stuff we pull from)
* [x]: create models for hats
* [x]: create admin registration for hat model in admin
* [x]: Create api_views
* [x]: create location encoder, list hats encoder, detail hats encoder
* [x]: create api function to list hats route from where we can (get and post)
* [x]: create api function to detail hat route from where we can (get, put, delete)
* [x]: configure api_urls for hats
* [x]: configure api path within urls for hats_project
* [x]: make sure to add hats api config in installed apps
* [x]: checked out docker to figure out route for wardrobe project, add local host and wardrobe-api to allowed hosts
* [x]: Hats API: the RESTful API to interact with Hat resources
* [x]: Hats Poller: the poller to poll the Wardrobe API for Location resources
* [x]: Hatform.js completed
* [x]: hatslist.js completed
* [x]: index.js updated
* [x]: nav.js updated

needed:
- Database: the PostgreSQL database that will hold the data of all of the microservices
- docker volume create pgdata???? is this the command that provides the database? (i think so because it generates the db/volume thing inside docker which is hosted locally)
- Hats Poller: the poller to poll the Wardrobe API for Location resources

Team:
* Person 1 - Lee Pham (Hats)
* Person 2 - Eric Lu (Shoes)

* []: create dev branches per team mate

## Design

## Shoes microservice
Planning/Designing for Shoes microservice -- Eric Lu
https://www.notion.so/Wardrobify-Project-6e354ecaba6a4a959d9476ad5e9a0958

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
