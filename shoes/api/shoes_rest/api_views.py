
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
import json


#import model for shoe and binVO
from .models import Shoe, BinVO

# encoder for binVOs  moving on top of shoeDetailEncoder so Django can trace it. 
class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
    ]

# encoder for listview
class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "color",
        "model_name",
        "bin",
        ]
    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


# encoder for detailview
class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "shoeURL",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }



# listview for shoes   (GET and POST)
@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET": # GET
        if bin_vo_id is not None:
           shoes = Shoe.objects.filter(bin=bin_vo_id) 
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
    )
    else: #POST
        content = json.loads(request.body)
        print("===========================shoes===================",content)    # debugging print
        if "bin" in content:
            try:
                bin_href = content["bin"]
                bin = BinVO.objects.get(import_href=bin_href)
                print("binbinbinbinbinbinbinbinbin")
                print(bin.closet_name)
                content["bin"] = bin
            except BinVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid bin id!!!!!!!!!!!!!!!!!!!!!!!"},  # debugging print
                    status=400,
                )

        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
            )




#detailview for a shoe ("GET", "PUT","DELETE")

@require_http_methods(["GET", "PUT","DELETE"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response= JsonResponse({"message":"please add shoe, currently doesn't exist"})
            response = 404
            return response
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "bin" in content:
                bin_href = content["bin"]
                bin = BinVO.objects.get(import_href=bin_href)
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=400,
            )
        Shoe.objects.filter(id=pk).update(**content)
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,)
        
    else: #DELETE
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"shoe has been deleted": count > 0})