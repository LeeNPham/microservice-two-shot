from django.urls import path
from.api_views import (api_list_shoes, api_show_shoe)
urlpatterns = [
    #path for list of shoes
    path("shoes/",api_list_shoes,name="api_list_shoes"),
    #path for detail of one shoe
    path("shoes/<int:pk>/",api_show_shoe,name="api_show_shoe"),
    #path for show list of shoes in a bin
    path("bins/<int:bin_vo_id>/shoes/",api_list_shoes,name="api_show_shoes_bins")
]