import React from 'react';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            manufacturer: '',
            modelName: '',
            color: '',
            shoeURL: '',
            bins: []
        }
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handleModel_nameChange = this.handleModel_nameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleshoeURLChange = this.handleshoeURLChange.bind(this);
        this.handleBinChange = this.handleBinChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.model_name = data.modelName;
        data.shoeURL = data.shoeURL;
        delete data.modelName;
        delete data.url;
        delete data.shoeURL;
        delete data.bins;
        console.log(data)

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);

        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe)

            const cleared = {
                manufacturer: '',
                modelName: '',
                color: '',
                shoeURL: '',
                bin: '',
            }
            this.setState(cleared);
        }
    }

    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({manufacturer: value});
    }

    handleModel_nameChange(event) {
        const value = event.target.value;
        this.setState({modelName: value});
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value});
    }

    handleshoeURLChange(event) {
        const value = event.target.value;
        this.setState({shoeURL: value});
    }

    handleBinChange(event) {
        const value = event.target.value;
        this.setState({bin: value});
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);
        //console.log(response)
        if (response.ok) {
            const data = await response.json();
            console.log("bin data=========================",data);
            this.setState({bins: data.bins});
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create A New Shoe</h1>
                    <form onSubmit={this.handleSubmit} id="create-shoe-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleManufacturerChange} value={this.state.manufacturer} placeholder="manufacturer" required name="manufacturer" id="manufacturer" className="form-control" />
                        <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleModel_nameChange} value={this.state.modelName} placeholder="model_name" required name="model_name" id="model_name" className="form-control" />
                        <label htmlFor="model_name">Model Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required name="color" id="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="mb-3">
                        <input onChange={this.handleshoeURLChange} value={this.state.shoeURL} placeholder="Picture" type="url" name="shoeURL" id="shoeURL" className="form-control"></input>
                    </div>
                    <div className="mb-3">
                        <select onChange={this.handleBinChange} value={this.state.bin} required name="bin" id="bin" className="form-select">
                        <option value="">Choose A bin</option>
                        {this.state.bins.map(bin => {
                            return (
                                <option key={bin.closet_name} value={bin.href}>
                                    {bin.closet_name}
                                </option>
                            );
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        )
    }
}

export default ShoeForm;
