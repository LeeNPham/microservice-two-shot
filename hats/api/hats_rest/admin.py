from django.contrib import admin
from .models import Hat

#admin.site.register(Hat) // I'm not really sure which of the two formats is more heavily used honestly. 

@admin.register(Hat)
class HatAdmin(admin.ModelAdmin):
    pass
